import os
from pathlib import Path


env = Environment(ENV = {'PATH' : os.environ['PATH']})
docs = Path("docs")

for book in docs.iterdir():
    if book.is_dir():
        for file in book.iterdir():
            if file.is_file():

                # print (file.suffix)
                # if file.suffix.lower() == ".json":
                #     env.Command(f"{file.parent}/{file.stem}.md", [f"{file}", "templates/meta.md"], action="""jinjafy templates/meta.md < $SOURCE > $TARGET""")
                #     env.Command(f"{file.parent}/{file.stem}.html", f"{file.parent}/{file.stem}.md", action="""pandoc --from markdown --to html --css ../../meta.css --standalone --section-divs $SOURCE -o $TARGET""")
                #     env.Command(f"{file.parent}/{file.stem}.epub", f"{file.parent}/{file.stem}.md", action="""pandoc --from markdown --to epub --css meta.css $SOURCE -o $TARGET""")

                # Ensure png => jpg
                if file.suffix.lower() == ".png":
                    jpg = file.parent/ f"{file.stem}.jpg"
                    if not jpg.exists():
                        env.Command(str(jpg), str(file), action="""convert $SOURCE $TARGET""")
                            