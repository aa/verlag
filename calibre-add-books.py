# https://manual.calibre-ebook.com/db_api.html
# run with calibre-debug command installed by calibre
#

from calibre.library import db
import json
from pathlib import Path
from calibre.ebooks.metadata.book.base import Metadata, SIMPLE_GET
from xml.etree import ElementTree as ET 
import html5lib

def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

"""
add_books

Add the specified books to the library. Books should be an iterable of 2-tuples, each 2-tuple of the form (mi, format_map) where mi is a Metadata object and format_map is a dictionary of the form {fmt: path_or_stream}, for example: {'EPUB': '/path/to/file.epub'}.


SIMPLE_GET:
frozenset({'manifest', 'rights', 'uuid', 'timestamp', 'publication_type', 'thumbnail', 'spine', 'series', 'lpath', 'formats', 'title_sort', 'title', 'guide', 'authors', 'rating', 'author_sort', 'cover_data', 'comments', 'languages', 'toc', 'tags', 'book_producer', 'author_sort_map', 'pubdate', 'last_modified', 'db_id', 'size', 'publisher', 'cover', 'series_index', 'user_categories', 'mime', 'author_link_map', 'device_collections', 'application_id', 'identifiers'})

"""

# print (SIMPLE_GET)

db = db('../calibre').new_api

# for book_id in db.all_book_ids():
# 	print (book_id)
# 	d = db.get_metadata(book_id)
# 	print (d)

with open("verlag.json") as fin:
    books_meta = json.load(fin)
books_meta = books_meta['items']

docs_path = Path("docs")

books_to_add = []

"""
authors:
- Ramírez, Catherine S.
date_added: '2007-10-01'
download: https://www.constantvzw.org/verlag/IMG/doc/Catherine_S_Ramirez.doc
download_status: 200
language: English
license: creativecommons.org/licenses/by-nc-sa/3.0/
projects:
- Stitch and Split
published_in:
- AS 178 Selves and Territories in Science Fiction
status: Selected text
subtitle: Humanism and Infrahumanism
themes:
- Science (-) Fiction
title: '"She Did Not Own Herself Any Longer". Slavery and the Promise of Humanism
  in Octavia E. Butler’s Science Fiction'
type: text
url: https://www.constantvzw.org/verlag/spip.php?page=article&id_article=41&mot_filtre=4&id_lang=0&debut_source_material=0
year: '2004'

authors:
- Zummer, Thomas
date_added: '2007-10-01'
download: http://data.constantvzw.org/s-a-s/16_zummer.pdf
download_status: 404
language: English
license: creativecommons.org/licenses/by-nc-sa/3.0/
projects:
- Stitch and Split
status: Resource
themes:
- Body and technology
title: 'Arrestments: Corporeality and Mediation'
type: file
url: https://www.constantvzw.org/verlag/spip.php?page=article&id_article=38&mot_filtre=4&id_lang=0&debut_source_material=0
wayback: http://web.archive.org/web/20110814075540id_/http://data.constantvzw.org/s-a-s/16_zummer.pdf
wayback_view: http://web.archive.org/web/20110814075540/http://data.constantvzw.org/s-a-s/16_zummer.pdf
"""

LANGS = {}
LANGS['Español'] = "Spanish"
LANGS['Nederlands'] = "Dutch"
LANGS['Français'] = "French"
LANGS['English'] = "English"

def unsplit_author_name (text):
    if "," in text:
        last, first = text.split(",", 1)
        return f"{first} {last}"
    return text

for book in books_meta:
    bookpath = docs_path / book['md5']
    if 'authors' in book:
        authors = [unsplit_author_name(x) for x in book['authors']]
    else:
        authors = []
    if bookpath.exists():
        files = [x for x in bookpath.iterdir() if x.is_file()]
    else:
        files = []
    covers = [x for x in files if x.suffix.lower() == ".jpg"]
    # pubs = [x for x in files if x.suffix.lower() in (".epub", ".pdf") and x.name != "meta.epub"]
    pubs = [x for x in files if x.suffix.lower() not in (".jpg", ".png")]
    pubs = [x for x in files if not x.name.startswith("verlag.")]
    # print (bookpath, pubs)
    cover_data = None
    if covers:
        with open(covers[0], "rb") as fin:
            cover_data = ('jpeg', fin.read())
            
    # Make the "format map" (format files as dict with TYPE as keys)
    formats = {}
    if pubs:
        for p in pubs:
            formats[p.suffix[1:].upper()] = str(p)
            # if p.suffix.lower() == ".epub":
            #     formats["EPUB"] = str(p)
            #     # formats["EPUB"] = f"{book['path']}/{p.name}"
            # elif p.suffix.lower() == ".pdf":
            #     formats["PDF"] = str(p)
    if "EPUB" not in formats:
        # formats["EPUB"] = str(docs_path / f"{book['md5']}.epub")
        formats["EPUB"] = str(bookpath / "verlag.epub")
    if "PDF" not in formats:
        # formats["PDF"] = str(docs_path / f"{book['md5']}.pdf")
        formats["PDF"] = str(bookpath / "verlag.pdf")
    if "MD" not in formats:
        # formats["MD"] = str(docs_path / f"{book['md5']}.md")
        formats["MD"] = str(bookpath / "verlag.md")

    # Make the metadata object    
    md = Metadata(book['title'], authors)
    if 'year' in book:
        md.pubdate = f"{book['year']}"

    # is comments the description!
    if cover_data is not None:
        md.cover_data = cover_data

    tags = ["Verlag"]
    if 'projects' in book:
        tags.extend(book['projects'])
    if 'themes' in book:
        tags.extend(book['themes'])
    if 'status' in book:
        tags.append(book['status'])
    if 'type' in book:
        tags.append(book['type'])
    # if 'download_status' in book:
    #     tags.append(f"{book['download_status']}")
    
    md.tags = tags
    
    comments = ""
    comments += f"""<p class="verlag">Republished from <a class="verlag" href="{book['url']}">Constant Verlag</a></p>"""
    if 'download' in book:
        comments += f"""<p class="download">Original download link: <a class="download" href="{book['download']}">{book['download']}</a>"""
        if 'download_status' in book and book['download_status'] != 200:
            comments += """ <a title="The link may be broken">⚠️</a>"""
        comments += """</p>"""
    if 'wayback' in book:
        comments += f"""<p class="wayback">An archival copy appears to be available: <a class="wayback" href="{book['wayback']}">Wayback machine on archive.org</a></p>"""
    if comments:
        md.comments = comments

    # # get the meta HTML
    # with open(bookpath / "meta.html") as fin:
    #     t = html5lib.parse(fin.read(), namespaceHTMLElements=False)
    #     body = t.find("./body")
    #     ET.tostring(body, method="html")
    #     meta_html = innerHTML(body)
    #     # print (meta_html)

    if 'language' in book:
        md.language = LANGS.get(book['language'], book['language'])
    books_to_add.append((md, formats))


results = db.add_books(books_to_add)    
print ("RESULTS of add_books")
print (results)

"""
Title               : Dune, Depolitization and Decolonizing the Future
Title sort          : Salman Sayyid
Author(s)           : Salman Sayyid [Sayyid]
Timestamp           : 2022-01-22T17:36:24+00:00
Published           : 0101-01-01T00:00:00+00:00
Comments            : <div>
<p><a href="https://constantvzw.org/site/-Stitch-And-Split,74-.html">Stich and Split</a></p></div>
"""
